# .gitignore по этому примеру: https://github.com/github/gitignore/blob/master/Terraform.gitignore
# Файлы, которые будут проигнорированы в будущем благодаря
# добавленному .gitignore
#
# 1. Локальная директория /.terraform вне зависимости от глубины вложения
#    Все файлы внутри директории /.terraform
# 2. Все файлы текущей директории с расширением .tfstate и
#    файлы имеющие в названии .tfstate. с любым расширением
# 3. Файл crash.log текущей директории
# 4. Все файлы текущей директории с расширением .tfvars
# 5. Файлы текущей директории override.tf и override.tf.json,
#    а так же все файлы текущей директории оканчивающиеся на _override.tf
#    и _override.tf.json
# 6. Файлы .terraformrc и terraform.rc текущей директории
#
